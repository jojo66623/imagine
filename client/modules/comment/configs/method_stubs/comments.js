export default function ({Collections, Meteor}) {
    Meteor.methods({
        'comment.create'(_id, postId, text, currentUserId) {
            const saving = true;
            const createdAt = new Date();
            const authorId = currentUserId;
            Collections.Comments.insert({
                _id, postId, text, saving, createdAt, authorId
            });
        }
    });
}
