import React from 'react';

const CommentView = ({comment, author}) => (
    <div>
        <b> { author.username }: </b> { comment.text }
        { comment.saving ? '...' : null }
    </div>
);

export default CommentView;