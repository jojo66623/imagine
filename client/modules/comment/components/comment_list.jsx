import React from 'react';
import CommentForm from '../containers/comment_form';
import CommentView from '../containers/comment_view';

const CommentList = ({comments, postId, currentUser}) => (
    <div className="comments">    
        <ul className="comment-list list-group">
            { comments.map(comment => (
                <li key={comment._id} className="comment list-group-item">
                    <CommentView comment={comment} />
                </li>
            ))}
        </ul>
        { currentUser ? <CommentForm postId={postId} /> : ''}
    </div>
);

export default CommentList;
