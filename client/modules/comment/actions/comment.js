export default {
    create({Meteor, LocalState}, postId, text, userId) {
        if (!text) {
            return LocalState.set('CREATE_COMMENT_ERROR', 'Comment text is required.');
        }

        if (!postId) {
            return LocalState.set('CREATE_COMMENT_ERROR', 'postId is required.');
        }

        if (!userId) {
            return LocalState.set('CREATE_COMMENT_ERROR', 'You must be logged.');
        }

        LocalState.set('CREATE_COMMENT_ERROR', null);

        const id = Meteor.uuid();
        Meteor.call('comment.create', id, postId, text, userId, (err) => {
            if (err) {
                return LocalState.set('CREATE_COMMENT_ERROR', err.message);
            }
        });
    },

    clearErrors({LocalState}) {
        return LocalState.set('CREATE_COMMENT_ERROR', null);
    }
};
