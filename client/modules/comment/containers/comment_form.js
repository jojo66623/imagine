import {useDeps, composeWithTracker, composeAll} from 'mantra-core';
import Component from '../components/comment_form.jsx';

export const composer = ({context, clearErrors}, onData) => {
    const {LocalState} = context();
    const error = LocalState.get('CREATE_COMMENT_ERROR');
    onData(null, {error});

    return clearErrors;
};

export const depsMapper = (context, actions) => ({
    create: actions.comment.create,
    clearErrors: actions.comment.clearErrors,
    context: () => context
});

export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMapper)
)(Component);
