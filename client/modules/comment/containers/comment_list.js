import {useDeps, composeWithTracker, composeAll} from 'mantra-core';
import Component from '../components/comment_list.jsx';

export const composer = ({context, clearErrors, postId}, onData) => {
    const {Meteor, Collections} = context();
    if (Meteor.subscribe('comment.list', postId).ready()) {
        const options = {
            sort: {createdAt: 1}
        };
        const comments = Collections.Comments.find({postId}, options).fetch();
        const currentUser = Meteor.user();
        onData(null, {comments, currentUser});
    } 
    else {
        onData();
    }
};

export default composeAll(
    composeWithTracker(composer),
    useDeps()
)(Component);
