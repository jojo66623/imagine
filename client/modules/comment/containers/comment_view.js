import {useDeps, composeWithTracker, composeAll} from 'mantra-core';
import Component from '../components/comment_view.jsx';

export const composer = ({context, clearErrors, comment}, onData) => {
    const {Meteor, Collections} = context();

    if (Meteor.subscribe('user.view', comment.authorId).ready()) {    
        const author = Meteor.users.findOne(comment.authorId);
        onData(null, {comment, author});
    } 
    else {
        onData();
    }
};

export default composeAll(
    composeWithTracker(composer),
    useDeps()
)(Component);
