import React from 'react';
import {mount} from 'react-mounter';

import MainLayout from './components/main_layout.jsx';
import Login from './components/login.jsx';
import MessageList from '../message/containers/message_list.js';

export default function (injectDeps, {FlowRouter}) {
    const MainLayoutCtx = injectDeps(MainLayout);

    FlowRouter.route('/messages/:userId', {
        name: 'message.list',
        action({userId}) {
            mount(MainLayoutCtx, {
                content: () => (<MessageList userId={userId} />)
            });
        }
    });

    FlowRouter.route('/login', {
        name: 'user.login',
        action() {
            mount(Login);
        }
    });

    FlowRouter.triggers.enter([userRedirect]);
}

function isUserRegistred(context) {
    const currentUser = Meteor.user();
    if(currentUser && !currentUser.profile.register){
        FlowRouter.go('user.edit', {userId: currentUser._id});
    }
}

function userRedirect(context) {
    const currentUser = Meteor.user();
    const loggingIn = Meteor.loggingIn();
    if (!currentUser && !loggingIn){
        Meteor.logout(
            FlowRouter.go('user.login')
        );
    }
    else {
        if (currentUser && !currentUser.profile.register){
            FlowRouter.go('user.edit', {userId: currentUser._id});
        }
    }
}
