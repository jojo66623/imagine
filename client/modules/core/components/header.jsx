import React from 'react';
import Navigation from './navigation.jsx';

const Header = () => (
    <div>
        <header className="text-center container">
            <h1>Imagine</h1>
            <Navigation />
        </header>
    </div>
);

export default Header;
