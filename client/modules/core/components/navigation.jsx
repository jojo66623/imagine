import React from 'react';
import UserWidget from '../../user/containers/user_widget';

const Navigation = () => (
    <nav className="navbar navbar-default" role="navigation">
        <div className="collapse navbar-collapse">
            <ul className="nav nav-justified">
                <li><a href={FlowRouter.path('post.list')}>Home</a></li>
                <li><a href={FlowRouter.path('user.view', {userId: Meteor.userId()})}>My profile</a></li>
                <li> <UserWidget /></li>
            </ul>
        </div>
    </nav>
);

export default Navigation;
