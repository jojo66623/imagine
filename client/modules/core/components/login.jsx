import React from 'react';
import ReactDOM from 'react-dom';

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            register: false
        }
    }

    submit(e) {
        e.preventDefault();
        const username = this.refs.username.value;
        const password = this.refs.password.value;

        if(this.state.register){
            const email = this.refs.email.value;
            Accounts.createUser({username, email, password}, function(err){
                if (err){
                    alert("Un problème est survenu, votre compte n'a pas pu être créé.");
                    throw new Meteor.Error("Registration failed");
                }
                else {
                    alert("Merci d'avoir souscrit chez nous, un email de confirmation va vous être envoyé pour valider votre compte.");
                    FlowRouter.go('user.edit', {userId: Meteor.userId()});
                }
            });
        }
        else {
            Meteor.loginWithPassword(username, password, function(err){
                if (err){
                  alert("L'identifiant ou le mot de passe n'est pas correct.\nSi vous n'avez pas de compte veuillez en créer un en cliquant sur 'Register'");
                    throw new Meteor.Error("Login failed");
                }
                else {

                    const currentUser = Meteor.user();
                    if(currentUser && !currentUser.profile.register){
                        FlowRouter.go('user.edit', {userId: currentUser._id});
                    }
                    else{

                        FlowRouter.go('post.list');
                    }
                }
            });
        }
    }

    facebookLogin(e) {
        e.preventDefault();
        Meteor.loginWithFacebook({}, function(err){
            if (err) {
                throw new Meteor.Error("Facebook login failed");
            }
            else {
                const currentUser = Meteor.user();
                if(currentUser && !currentUser.profile.register){
                    FlowRouter.go('user.edit', {userId: currentUser._id});
                    }
                else{
                    FlowRouter.go('post.list');
                }
            }
        })
    }

    toggleRegister() {
        this.setState({
            register: !this.state.register
        });
    }

    render() {
        return (
            <div>
                <h2 className="text-center">{this.state.register ? "Register" : "Login"}</h2>
                <form onSubmit={this.submit.bind(this)}>
                    <input type="text" className="form-control" ref="username" placeholder={this.state.register ? "Username" : "Username or email"} />
                    {this.state.register ? <input type="email" className="form-control" ref="email" placeholder="Email" /> : ''}
                    <input type="password" className="form-control" ref="password" placeholder="Password"/>
                    <input type="submit" className="btn btn-primary" value={this.state.register ? "Sign up" : "Sign in"} />
                </form>
                <button id="facebook-login" className="btn btn-primary" onClick={this.facebookLogin.bind(this)}>
                    <i className="fa fa-facebook-square"></i> Login with Facebook
                </button>
                <a onClick={this.toggleRegister.bind(this)}>{this.state.register ? "Login" : "Register"}</a>
            </div>
        );
    }
};

export default Login;
