import React from 'react';
import Header from './header.jsx';
import MessageEngine from '../../message/containers/message_engine';

const Layout = ({content = () => null, left_sidebar = () => null }) => (
    <div>
        <Header />
        <div className='main-container container'>
            <div className='row'>
                { left_sidebar() ? <aside className='col-sm-3'>{ left_sidebar() }</aside> : '' }
                <section className={left_sidebar() ? `col-sm-6` : `col-sm-9`} >
                    { content() }
                </section>

                <aside className='col-sm-3 '>
                    <MessageEngine />
                </aside>

            </div>
        </div>
        <footer className="footer container text-center">
            <small>Built with <a href='https://github.com/kadirahq/mantra'>Mantra</a> &amp; Meteor.</small>
        </footer>
    </div>
);

export default Layout;
