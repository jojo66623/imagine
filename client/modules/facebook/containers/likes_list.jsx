import Component from '../components/likes_list.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

export const composer = ({context, userId}, onData) => {
    const {Meteor, Collections} = context();
    if (Meteor.subscribe('facebook.data',userId).ready()) {
        const fbData = Collections.FacebookData.findOne({userId: userId});
        onData(null, {fbData});
    } 
    else {
        onData();
    }
};

export default composeAll(
    composeWithTracker(composer),
    useDeps()
)(Component);