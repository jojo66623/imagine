import React from 'react';

const LikesList = ({fbData}) => ( 
    <div>
        { fbData && fbData.likes && fbData.likes.length > 0 ?
            <ul className="likes list-group">
                { fbData.likes.map(like => (
                    <li key={like.id} className="col-sm-3 col-lg-2 col-xs-6 list-group-item text-center">
                        <a href={like.link} target='_blank' title={like.name}>
                            <figure>
                                <div className="image"><img src={like.picture ? like.picture.data.url : ''} /></div>
                                <figcaption className="text-capitalize">{like.name.substring(0, 12).toLowerCase()}{like.name.length > 12 ? '...' : ''}</figcaption>
                                <figcaption className="text-capitalize small">{like.category.substring(0, 14).toLowerCase()}{like.category.length > 14 ? '...' : ''}</figcaption>
                            </figure>
                        </a>
                    </li>
                ))}
            </ul> 
            : ''
        }
    </div>
);

export default LikesList;