import React from 'react';

class FacebookConnect extends React.Component {
    render (user) {
        return( 
            <button className="btn btn-primary" onClick={this.facebookLogin.bind(this)}>Connect to Facebook</button>
        )
    }

    facebookLogin(e) {
        e.preventDefault();
        Meteor.loginWithFacebook({}, function(err){
            if (err) {
                throw new Meteor.Error("Facebook login failed");
            }
        });
    }
};

export default FacebookConnect;