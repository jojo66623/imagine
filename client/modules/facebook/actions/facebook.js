export default {
    update({Meteor, LocalState, FlowRouter}, userId, username, email, gender, partner_gender, pictures) {

        if (!username) {
            return LocalState.set('SAVING_ERROR', 'Username is required!');
        }

        if (!email) {
            return LocalState.set('SAVING_ERROR', 'Email is required!');
        }

        LocalState.set('SAVING_ERROR', null);

        Meteor.call('user.register', userId, username, email, gender, partner_gender, pictures, (err) => {
            if (err) {
                return LocalState.set('SAVING_ERROR', err.message);
            }
        });

        FlowRouter.go('user.view', {userId: userId});
    },

    clearErrors({LocalState}) {
        return LocalState.set('SAVING_ERROR', null);
    }
};
