export default {
    update({Meteor, LocalState, FlowRouter}, userId, username, email, pictures) {

        if (!username) {
            return LocalState.set('SAVING_ERROR', 'Username is required!');
        }

        if (!email) {
            return LocalState.set('SAVING_ERROR', 'Email is required!');
        }

        LocalState.set('SAVING_ERROR', null);

        Meteor.call('user.register', userId, username, email, pictures, (err) => {
            if (err) {
                return LocalState.set('SAVING_ERROR', err.message);
            }
        });

        FlowRouter.go('user.view', {userId: userId});
    },

    get({Meteor, LocalState, FlowRouter}, userId) {
        return Meteor.users.findOne(userId);
    },

    clearErrors({LocalState}) {
        return LocalState.set('SAVING_ERROR', null);
    }
};
