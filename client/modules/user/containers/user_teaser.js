import Component from '../components/user_teaser.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

export const composer = ({context, userId}, onData) => { 
    if(Meteor.subscribe('user.view', userId).ready()) {
        const user = Meteor.users.findOne(userId);
        onData(null, {user});
    }
    else {
        onData();
    }
};

export default composeAll(
    composeWithTracker(composer),
    useDeps()
)(Component);