import Component from '../components/user_list.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

export const composer = ({context}, onData) => {
    const {Meteor, Collections} = context();
    if (Meteor.subscribe('user.list').ready()) {
        const users = Meteor.users.find({ '_id': { $ne: Meteor.userId() } } );
        onData(null, {users});
    }
};

export default composeAll(
    composeWithTracker(composer),
    useDeps()
)(Component);