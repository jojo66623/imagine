import Component from '../components/user_form.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

export const composer = ({context, clearErrors, userId}, onData) => {
    
    const {Meteor, LocalState} = context();
    if(Meteor.subscribe('user.view', userId).ready()) {
        const user = Meteor.users.findOne(userId);
        const error = LocalState.get('CREATE_COMMENT_ERROR');
        onData(null, {user, error});

        return clearErrors;
    }
    else {
        onData();
    }
};

export const depsMapper = (context, actions) => ({
    update: actions.user.update,
    clearErrors: actions.user.clearErrors,
    context: () => context
});

export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMapper)
)(Component);
