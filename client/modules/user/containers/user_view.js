import Component from '../components/user_view.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

export const composer = ({context, userId}, onData) => {
  	if(Meteor.subscribe('user.view', userId).ready()) {
  		const user = Meteor.users.findOne(userId);
  		const currentUser = Meteor.user();
    	onData(null, {user, currentUser});
	}
	else {
  		onData();
	}
};

export default composeAll(
  	composeWithTracker(composer),
  	useDeps()
)(Component);
