import Component from '../components/user_widget.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

export const composer = ({context}, onData) => { 
    const {Meteor, Collections} = context();
    const currentUserId = Meteor.userId();
    if (Meteor.subscribe('message.list', currentUserId).ready()) {    
        const countUnread = Collections.Messages.find({toId: currentUserId, read: false}).count();
        onData(null, {countUnread, currentUserId});
    } 
    else {
        onData(null, {currentUserId});
    }  
};

export default composeAll(
    composeWithTracker(composer),
    useDeps()
)(Component);