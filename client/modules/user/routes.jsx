import React from 'react';
import {mount} from 'react-mounter';

import MainLayout from '../core/components/main_layout.jsx';
import UserForm from './containers/user_form.js';
import UserList from './containers/user_list.js';
import UserView from './containers/user_view.js';

export default function (injectDeps, {FlowRouter}) {
  const MainLayoutCtx = injectDeps(MainLayout);

  var userSection = FlowRouter.group({
    prefix: "/user"
  });

  userSection.route('/', {
    name: 'user.view.current',
    action() {
      mount(MainLayoutCtx, {
        content: () => (<UserView userId={this.userId} />)
      });
    }
  });

  userSection.route('/:userId', {
    name: 'user.view',
    action({userId}) {
      mount(MainLayoutCtx, {
        content: () => (<UserView userId={userId} />)
      });
    }
  });

  userSection.route('/:userId/edit', {
    name: 'user.edit',
    action({userId}) {
      mount(MainLayoutCtx, {
        content: () => (<UserForm userId={userId}/>)
      });
    }
  });

  FlowRouter.route('/users', {
    name: 'user.list',
    action() {
      mount(MainLayoutCtx, {
        content: () => (<UserList />)
      });
    }
  });
}


