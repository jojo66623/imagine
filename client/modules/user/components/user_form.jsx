import React from 'react';
import Dropzone from 'react-dropzone';

class UserForm extends React.Component {

    constructor(props) {
        super(props);
        const {user} = this.props;
        this.state = {
            username: (user.username && user.username.length > 0) ? user.username : (user.services.facebook.name ? user.services.facebook.name : ''),
            email: user.emails ? user.emails[0].address : (user.services.facebook.email ? user.services.facebook.email : ''),
            pictures: (user.profile && user.profile.pictures) ? user.profile.pictures : []
        }
    }

    save(e) {
        e.preventDefault();
        const {user, update} = this.props;
        const {username, email} = this.refs;
        const pictures = this.state.pictures;

        update(user._id, username.value, email.value, pictures);
    }

    handleChange(e,id) {
        this.setState({
            id: e.target.value
        });
    }

    onDrop(files) {
        var new_pictures = [];
        files.forEach(function(file){
            var uploader = new Slingshot.Upload("pictures");
            uploader.send(file, function (error, downloadUrl) {
                if (error) {
                    // Log service detailed response.
                    console.error('Error uploading', uploader.xhr.response);
                }
                else {
                    this.setState({
                        pictures: this.state.pictures.concat([downloadUrl])
                    });
                }
            }.bind(this))
        }.bind(this));
    }

    deleteImg(index) {
        let pictures = this.state.pictures;
        pictures.splice(index, 1);
        this.setState({
            pictures: pictures
        });
    }

    render() {
        const {user} = this.props;
        return (
            <div>
                <form id="register_form" onSubmit={this.save.bind(this)}>

                    <div className="form-group">
                        <Dropzone ref="dropzone" onDrop={this.onDrop.bind(this)}>
                            <div>Try dropping some files here, or click to select files to upload.</div>
                        </Dropzone>

                        { this.state.pictures && this.state.pictures.length > 0 ?
                            <div>
                                <h2>Your {this.state.pictures.length} pictures...</h2>
                                <div>
                                    { this.state.pictures.map((url, index) =>
                                        <div className="col-sm-3" key={url}>
                                            <img className="col-xs-12" src={url} />
                                            <a className="btn btn-danger" onClick={this.deleteImg.bind(this,index)}>Delete</a>
                                        </div>
                                    )}
                                </div>
                            </div>
                            : null
                        }
                    </div>

                    <div className="form-group">
                        <input className="form-control" type="text" ref="username" id="username" placeholder="username" required value={this.state.username} onChange={this.handleChange.bind(this,'username')} />
                    </div>

                    <div className="form-group">
                        <input className="form-control" type="email" ref="email" id="email" placeholder="email" required defaultValue={this.state.email} onChange={this.handleChange.bind(this,'email')} />
                    </div>

                    <input className="btn btn-primary" type="submit" value="Save" />

                </form>
            </div>
        );
    }
};

export default UserForm;
