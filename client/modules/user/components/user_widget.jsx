import React from 'react';
import ReactDOM from 'react-dom';
import UserTeaser from '../containers/user_teaser';

class UserWidget extends React.Component{
    logout(e){
        e.preventDefault();
        Meteor.logout(FlowRouter.go('user.login'))  
    }
    render() {
        const {currentUserId, countUnread} = this.props;
        return(
            <div className='userwidget'>
                { currentUserId ?
                    <div className="logged">
                        <a href={FlowRouter.path('user.view', {userId: Meteor.userId()})}>
                            <UserTeaser userId={currentUserId} />
                        </a>
                        <a href={FlowRouter.path('message.box')}>
                            <i className="fa fa-envelope" aria-hidden="true"></i>
                            <sup>{countUnread > 0 ? countUnread : ''}</sup>
                        </a>
                        <button className="btn btn-default" onClick={this.logout.bind(this)}>Log out</button>      
                    </div>  
                    : ''
                }     
            </div>
        )
    }
}

export default UserWidget;