import React from 'react';

const UserTeaser = ({user}) => (  
    <span>{ user.username }</span>
);

export default UserTeaser;
