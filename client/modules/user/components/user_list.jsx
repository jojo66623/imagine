import React from 'react';

const UserList = ({users}) => (
    <div className="navbar-default">
        <ul className="nav nav-pills nav-stacked userlist">
            { users.map(user => (
                <li key={user._id}>
                    <a href={FlowRouter.path('user.view', {userId: user._id})}>{user.username}</a>
                </li>
            ))}
        </ul>
    </div>
);

export default UserList;
