import React from 'react';
import LikesList from '../../facebook/containers/likes_list.jsx';
import FacebookConnect from '../../facebook/components/facebook_connect.jsx';

const UserView = ({user, currentUser}) => (
    <div>
        { user.saving ? <p>Saving...</p> : null }
            <h2>{user.username}</h2>
        <p>
            { user._id == currentUser._id ? 
                <a className="btn btn-primary" href={`/user/${user._id}/edit`}>Edit profile</a> 
                : <a className="btn btn-primary" href={`/messages/${user._id}`}>Send a message</a>
            }
        </p>
        { user._id == currentUser._id ? 
            ( user.services && user.services.facebook ?
                <LikesList userId={user._id} /> 
                : <FacebookConnect />
            ) 
            : ''
        }
    </div>
);

export default UserView;
