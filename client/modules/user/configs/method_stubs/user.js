import {check} from 'meteor/check';

export default function ({Meteor, Collections}) {
    Meteor.methods({
        'user.update'(userId, username, email, pictures) {
            check(userId, String);
            check(username, String);
            check(email, String);
            check(pictures, [String]);

            Meteor.users.update(userId, {
                $set: {
                    username: username,
                    'emails.0.address': email,
                    profile: {
                        pictures: pictures,
                        register: true
                    }
                }
            });
        }
    });

    Meteor.methods({
        'user.register'(userId, username, email, pictures) {
            check(userId, String);
            check(username, String);
            check(email, String);
            check(pictures, [String]);

            Meteor.users.update(userId, {
                $set: {
                    username: username,
                    'emails.0.address': email,
                    profile: {
                        pictures: pictures,
                        register: true
                    }
                }
            });
        }
    });
}
