import NewPost from '../components/post_form.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

export const composer = ({context, clearErrors}, onData) => {
    const {LocalState} = context();
    const error = LocalState.get('SAVING_ERROR');
    const userId = Meteor.userId();
    onData(null, {error, userId});

    return clearErrors;
};

export const depsMapper = (context, actions) => ({
    create: actions.post.create,
    clearErrors: actions.post.clearErrors,
    context: () => context
});

export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMapper)
)(NewPost);
