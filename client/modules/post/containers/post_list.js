import PostList from '../components/post_list.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

export const composer = ({context}, onData) => {
    const {Meteor, Collections} = context();
    if (Meteor.subscribe('post.list').ready()) {
        const options = {
            sort: {createdAt: -1}, 
            limit: 0
        };
        const posts = Collections.Posts.find({}, options).fetch();
        onData(null, {posts});
    }
    else {
        onData();
    }
};

export const depsMapper = (context, actions) => ({
    getPosts: actions.post.getPosts,
    context: () => context
});

export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMapper)
)(PostList);
