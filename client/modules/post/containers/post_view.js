import Component from '../components/post_view.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

export const composer = ({context, postId}, onData) => {
    const {Meteor, Collections, Tracker} = context();

    Meteor.subscribe('post.view', postId, () => {
        const post = Collections.Posts.findOne(postId);
        const author = Meteor.users.findOne(post.authorId);
        onData(null, {post,author});
    });
};

export default composeAll(
    composeWithTracker(composer),
    useDeps()
)(Component);
