import React from 'react';
import PostView from '../containers/post_view';
import PostForm from '../containers/post_form';

class PostList extends React.Component {
    constructor(props) {
        super(props);
        const {posts} = this.props;
        this.state = {
            hasMore: true,
            page: 0,
            nb_by_page: 10,
            posts: posts
        }
    }

    getPage() {
        const {getPosts} = this.props;
        const page = this.state.page;
        const nb_by_page = this.state.nb_by_page;
        const posts = getPosts(page,nb_by_page);
        return (
            <div key={page} className="page">
                { posts.map(post => (
                    <div key={post._id} className="post">
                        <PostView postId={post._id} />
                    </div>
                ))}
            </div>
        );
    }

    render() {
        const {posts} = this.props;
        return (
            <div className='postlist'>
                <PostForm />
                <ul className="list-group">
                    {posts.map(post => (
                        <li key={post._id} className="post list-group-item">
                            <PostView postId={post._id} />
                        </li>
                    ))}
                </ul>
            </div>
        );
    }

    loadMore () {
        const {getPosts} = this.props;
        const page = this.state.page;
        const nb_by_page = this.state.nb_by_page;
        const newposts = getPosts(page,nb_by_page);
        setTimeout(function () {
            this.setState({
                posts: this.state.posts.concat(newposts),
                hasMore: (newposts.length < this.state.nb_by_page),
                page: ++this.state.page
            });
        }.bind(this), 1000);
    }

    /*render() {
        const posts = this.state.posts;
        return (
            <div className='postlist'>
                <NewPost />
                <InfiniteScroll loader={<div className="loader">Loading ...</div>} loadMore={this.loadMore} hasMore={this.state.hasMore}>
                    {posts}
                </InfiniteScroll >
            </div>
        );
    }*/
}

export default PostList;
