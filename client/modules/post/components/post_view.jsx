import React from 'react';
import CommentList from '../../comment/containers/comment_list.js';

const PostView = ({post, author}) => (
    <div>
        { post.saving ? <p>Saving...</p> : null }
        { author.username } says :
        <p dangerouslySetInnerHTML={{__html: post.content}} />
        <div>
            <CommentList postId={post._id}/>
        </div>
    </div>
);

export default PostView;
