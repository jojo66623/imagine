import React from 'react';
import ReactQuill from 'react-quill';

class PostForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ''
        }
    }

    render() {
        const {error} = this.props;
        return (
            <div className="new-post">
                { error ? <p style={{color: 'red'}}>{error}</p> : null }
                <link rel="stylesheet" href="//cdn.quilljs.com/0.20.1/quill.snow.css" />
                <ReactQuill 
                    className="well" 
                    theme='snow' 
                    value={this.state.text} 
                    onChange={this.onTextChange.bind(this)} 
                />
                <button className="btn btn-primary" onClick={this.createPost.bind(this)}>Send</button>
            </div>
        );
    }

    onTextChange(value) {
        this.setState({ text:value });
    }

    createPost() {
        const {create, userId} = this.props;
        const text = this.state.text;

        create(text, userId);
        this.state.text= '';
    }
}

export default PostForm;
