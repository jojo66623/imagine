import React from 'react';
import {mount} from 'react-mounter';
import routes from '../core/routes.jsx'

import MainLayout from '../core/components/main_layout.jsx';
import PostList from './containers/post_list';
import PostView from './containers/post_view';
import PostForm from './containers/post_form';
import MessageEngine from '../message/containers/message_engine';

export default function (injectDeps, {FlowRouter}) {
    const MainLayoutCtx = injectDeps(MainLayout);

    FlowRouter.route('/', {
        name: 'post.list',
        action() {
            mount(MainLayoutCtx, {
                content: () => (<PostList />)
            });
        }
    });

    FlowRouter.route('/post/:postId', {
        name: 'post.view',
        action({postId}) {
            mount(MainLayoutCtx, {
                content: () => (<Post postId={postId} />)
            });
        }
    });

    FlowRouter.route('/post/add', {
        name: 'post.create',
        action() {
            mount(MainLayoutCtx, {
                content: () => (<PostForm />)
            });
        }
    });
}
