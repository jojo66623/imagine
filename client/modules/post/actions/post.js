export default {
    
    create({Meteor, LocalState, FlowRouter}, content, userId) {
        if ( ! content) {
            return LocalState.set('SAVING_ERROR', 'Content are required.');
        }

        if (! userId) {
            return LocalState.set('SAVING_ERROR', 'You have to be logged in to post.');
        }

        LocalState.set('SAVING_ERROR', null);

        const id = Meteor.uuid();

        Meteor.call('post.create', id, content, userId, (err) => {
            if (err) {
                return LocalState.set('SAVING_ERROR', err.message);
            }
        });
    },

    clearErrors({LocalState}) {
        return LocalState.set('SAVING_ERROR', null);
    },

    getPosts({Meteor, LocalState, FlowRouter}, page = 0, nb_by_page = 10) {
        if (Meteor.subscribe('post.list').ready()) {
            const options = {
                sort: {createdAt: -1}, 
                limit: nb_by_page, 
                skip: page*nb_by_page
            };
            return Collections.Posts.find({}, options).fetch();
        }
    }
};
