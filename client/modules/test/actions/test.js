export default {
  create({Meteor, LocalState, FlowRouter}, content, userId) {
    if ( ! content) {
      return LocalState.set('SAVING_ERROR', 'Content are required.');
    }

     if (! userId) {
      return LocalState.set('SAVING_ERROR', 'You have to be logged in to test.');
    }

    LocalState.set('SAVING_ERROR', null);

    const id = Meteor.uuid();
    // There is a method stub for this in the config/method_stubs
    // That's how we are doing latency compensation
    Meteor.call('tests.create', id, content, userId, (err) => {
      if (err) {
        return LocalState.set('SAVING_ERROR', err.message);
      }
    });
    //FlowRouter.go(`/test/${id}`);
  },

  clearErrors({LocalState}) {
    return LocalState.set('SAVING_ERROR', null);
  },

  gettests({Meteor, LocalState, FlowRouter}, page = 0, nb_by_page = 10) {
    if (Meteor.subscribe('tests.list').ready()) {
      const options = {
        sort: {createdAt: -1}, 
        limit: nb_by_page, 
        skip: page*nb_by_page
      };
      return Collections.tests.find({}, options).fetch();
    }
  }
};
