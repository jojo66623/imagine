import Component from '../components/test_view.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

export const composer = ({context, testId}, onData) => {
    const {Meteor, Collections, Tracker} = context();

    Meteor.subscribe('test.view', testId, () => {
        const test = Collections.tests.findOne(testId);
        const author = Meteor.users.findOne(test.authorId);
        onData(null, {test,author});
    });

    // support latency compensation
    //  we don't need to invalidate tracker because of the
    //  data fetching from the cache.
    /* const testFromCache = Tracker.nonreactive(() => {
    return Collections.tests.findOne(testId);
});

if (testFromCache) {
onData(null, {test: testFromCache});
} else {
onData();
}*/
};

export default composeAll(
    composeWithTracker(composer),
    useDeps()
)(Component);
