import Component from '../components/test_list.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

export const composer = ({context}, onData) => {
    const {Meteor, Collections} = context();
    if (Meteor.subscribe('tests.list').ready()) {
        const options = {
            sort: {createdAt: -1},
            limit: 0
        };
        const tests = Collections.tests.find({}, options).fetch();
        onData(null, {tests});
    }
    else {
        onData();
    }
};

export const depsMapper = (context, actions) => ({
    gettests: actions.tests.gettests,
    context: () => context
});

export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMapper)
)(Component);
