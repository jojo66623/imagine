const { describe, it } = global;
// import {expect} from 'chai';
// import {stub, spy} from 'sinon';
// import {composer, depsMapper} from '../newtest';

describe('containers.newtest', () => {
  describe('composer', () => {
    it('should get SAVING_ERROR from local state');
    it('should get SAVING_NEW_test from local state');
  });

  describe('depsMapper', () => {
    describe('actions', () => {
      it('should map tests.create');
      it('should map tests.clearErrors');
    });
    describe('context', () => {
      it('should map the whole context as a function');
    });
  });
});
