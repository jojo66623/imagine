const {describe, it} = global;
import {expect} from 'chai';
import {stub, spy} from 'sinon';
import {composer} from '../test';

describe('core.containers.test', () => {
  describe('composer', () => {
    const Tracker = {nonreactive: cb => cb()};
    const getCollections = (test) => {
      const Collections = {
        tests: {findOne: stub()}
      };
      Collections.tests.findOne.returns(test);
      return Collections;
    };

    it('should subscribe to the given testId via prop', () => {
      const Meteor = {subscribe: stub()};
      Meteor.subscribe.returns({ready: () => false});
      const Collections = getCollections();

      const context = () => ({Meteor, Tracker, Collections});
      const testId = 'dwd';
      const onData = spy();

      composer({context, testId}, onData);
      const args = Meteor.subscribe.args[0];
      expect(args.slice(0, 2)).to.deep.equal([
        'tests.single', testId
      ]);
    });

    describe('while subscribing', () => {
      it('should call just onData()', () => {
        const Meteor = {subscribe: stub()};
        const Collections = getCollections();

        const context = () => ({Meteor, Tracker, Collections});
        const testId = 'dwd';
        const onData = spy();

        composer({context, testId}, onData);
        expect(onData.args[0]).to.deep.equal([]);
      });
    });

    describe('after subscribed', () => {
      it('should find the test and send it to onData', () => {
        const Meteor = {subscribe: stub()};

        const test = {_id: 'test'};
        const Collections = getCollections(test);

        const context = () => ({Meteor, Collections, Tracker});
        const testId = 'dwd';
        const onData = spy();

        composer({context, testId}, onData);
        expect(onData.args[0]).to.deep.equal([ null, {test} ]);
      });
    });
  });
});
