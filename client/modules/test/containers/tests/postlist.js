const { describe, it } = global;
import {expect} from 'chai';
import {stub, spy} from 'sinon';
import {composer} from '../testlist';

describe('core.containers.testlist', () => {
  describe('composer', () => {
    it('should subscribe to tests.list', () => {
      const Meteor = {subscribe: stub()};
      Meteor.subscribe.returns({ready: () => false});

      const context = () => ({Meteor});
      const onData = spy();

      composer({context}, onData);
      expect(Meteor.subscribe.args[0]).to.deep.equal([
        'tests.list'
      ]);
    });

    describe('after subscribed', () => {
      it('should fetch data from all tests & pass to onData', () => {
        const Meteor = {subscribe: stub()};
        Meteor.subscribe.returns({ready: () => true});

        const tests = [ {_id: 'aa'} ];
        const Collections = {tests: {find: stub()}};
        Collections.tests.find.returns({fetch: () => tests});

        const context = () => ({Meteor, Collections});
        const onData = spy();

        composer({context}, onData);
        expect(onData.args[0]).to.deep.equal([ null, {tests} ]);
      });
    });
  });
});
