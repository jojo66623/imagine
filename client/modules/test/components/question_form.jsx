import React from 'react';

class QuestionForm extends React.Component {
    render() {
        const {error} = this.props;
        return (
            <div>
                { error ? this._renderError(error) : null }
                <textarea rows={ this.state.active ? 2 : 1}  className="form-control" ref='text' placeholder='Enter your question here.' onFocus={this.onFocus.bind(this)} onBlur={this.onBlur.bind(this)}>
                </textarea>
                { this.state.active ? <button className="btn btn-primary" onClick={this._create.bind(this)}>Add Question</button> : '' }
            </div>
        );
    }

    _create() {
        const text = this.refs.text.value;
        const {create, postId} = this.props;
        const currentUserId = Meteor.userId();
        create(postId, text, currentUserId);
        this.refs.text.value = '';
        this.setState({
            active: 0
        });
    }

    _renderError(error) {
        return (
            <div className='error'>
                {error}
            </div>
        );
    }

    constructor(props) {
        super(props);
        this.state = {
            active: 0,
        }
    }

    onBlur() {
        if(this.refs.text.value == ''){
            this.setState({
                active: 0
            });
        }
    }

    onFocus() {
        this.setState({
            active: 1
        });
    }
}

export default QuestionForm;
