import React from 'react';
import CommentList from '../../comment/containers/comment_list.js';

const TestView = ({test, author}) => (
    <div>
        { test.saving ? <p>Saving...</p> : null }
        { author.username } says :
        <p dangerouslySetInnerHTML={{__html: test.content}} />
        <div>
            <CommentList testId={test._id}/>
        </div>
    </div>
);

export default TestView;
