import React from 'react';
import ReactQuill from 'react-quill';

class TestForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ''
        }
    }

    render() {
        const {error, questions, testId} = this.props;
        return (
            <div className="new-test">
                {error ? <p style={{color: 'red'}}>{error}</p> : null}
                <link rel="stylesheet" href="//cdn.quilljs.com/0.20.1/quill.snow.css" />
                <ReactQuill
                    className="well"
                    theme='snow'
                    value={this.state.text}
                    onChange={this.onTextChange.bind(this)}
                />
                <button className="btn btn-primary" onClick={this.createtest.bind(this)}>
                    Send
                </button>
                <div className="comments">
                    <ul className="comment-list list-group">
                        { questions.map( question => (
                            <li key={question._id} className="question list-group-item">
                                <QuestionView question={question} />
                            </li>
                        ))}
                    </ul>
                    <QuestionForm testId={testId} />
                </div>
            </div>
        );
    }

    onTextChange(value) {
        this.setState({ text: value });
    }

    _create() {
        const {create, userId} = this.props;
        const text = this.state.text;

        create(text, userId);
        this.state.text = '';
    }
}

export default TestForm;
