import React from 'react';
import TestView from '../containers/test_view';
import TestForm from '../containers/test_form';

class TestList extends React.Component {
    constructor(props) {
        super(props);
        const {tests} = this.props;
        this.state = {
            hasMore: true,
            page: 0,
            nb_by_page: 10,
            tests: tests
        }
    }

    getPage() {
        const {gettests} = this.props;
        const page = this.state.page;
        const nb_by_page = this.state.nb_by_page;
        const tests = gettests(page,nb_by_page);
        return (
            <div key={page} className="page">
                { tests.map(test => (
                    <div key={test._id} className="test">
                        <TestView testId={test._id} />
                    </div>
                ))}
            </div>
        );
    }

    render() {
        const {tests} = this.props;
        return (
            <div className='testlist'>
                <Newtest />
                <ul className="list-group">
                    { tests.map(test => (
                        <li key={test._id} className="test list-group-item">
                            <TestView testId={test._id} />
                        </li>
                    ))}
                </ul>
            </div>
        );
    }

    loadMore () {
        const {gettests} = this.props;
        const page = this.state.page;
        const nb_by_page = this.state.nb_by_page;
        const newtests = gettests(page,nb_by_page);
        setTimeout(function () {
            this.setState({
                tests: this.state.tests.concat(newtests),
                hasMore: (newtests.length < this.state.nb_by_page),
                page: ++this.state.page
            });
        }.bind(this), 1000);
    }

    /*render() {
    const tests = this.state.tests;
    return (
    <div className='testlist'>
    <Newtest />
    <InfiniteScroll loader={<div className="loader">Loading ...</div>} loadMore={this.loadMore} hasMore={this.state.hasMore}>
    {tests}
    </InfiniteScroll >
    </div>
);
}*/
}

export default TestList;
