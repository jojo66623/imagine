import {check} from 'meteor/check';

export default function ({Meteor, Collections}) {
    Meteor.methods({
        'test.create'(_id, content, userId) {
            check(_id, String);
            check(content, String);
            check(userId, String);

            const createdAt = new Date();
            const authorId = userId;
            const test = {
                _id, content, createdAt, authorId,
                saving: true
            };

            Collections.tests.insert(test);
        }
    });
    Meteor.methods({
        'test.getTests'(page = 0, nb_by_page = 10) {
            check(page, Number);
            check(nb_by_page, Number);

            const options = {
                sort: {createdAt: -1},
                limit: nb_by_page,
                skip: page*nb_by_page
            };
            return Collections.tests.find({}, options).fetch();
        }
    });
}
