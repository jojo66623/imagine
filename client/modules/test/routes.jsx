import React from 'react';
import {mount} from 'react-mounter';

import MainLayout from '../core/components/main_layout.jsx';
import TestList from './containers/test_list';
import TestView from './containers/test_view';
import TestForm from './containers/test_form';

export default function (injectDeps, {FlowRouter}) {
    const MainLayoutCtx = injectDeps(MainLayout);

    FlowRouter.route('/tests', {
        name: 'test.list',
        action() {
            mount(MainLayoutCtx, {
                content: () => (<TestList />)
            });
        }
    })
    .route('/test/:testId', {
        name: 'test.view',
        action({testId}) {
            mount(MainLayoutCtx, {
                content: () => (<TestView testId={testId} />)
            });
        }
    })
    .route('/test/:testId/edit', {
        name: 'test.edit',
        action({testId}) {
            mount(MainLayoutCtx, {
                content: () => (<TestForm testId={testId} />)
            });
        }
    })
    .route('/test/add', {
        name: 'test.add',
        action() {
            mount(MainLayoutCtx, {
                content: () => (<TestForm />)
            });
        }
    });
}
