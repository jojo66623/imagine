export default {
    create({Meteor, LocalState}, userId, text, currentUserId) {
        if (!text) {
            return LocalState.set('CREATE_COMMENT_ERROR', 'Message text is required.');
        }

        if (!userId) {
            return LocalState.set('CREATE_COMMENT_ERROR', 'userId is required.');
        }

        if (!currentUserId) {
            return LocalState.set('CREATE_COMMENT_ERROR', 'You must be logged.');
        }

        LocalState.set('CREATE_COMMENT_ERROR', null);

        const id = Meteor.uuid();
        Meteor.call('message.create', id, userId, text, currentUserId, (err) => {
            if (err) {
                return LocalState.set('CREATE_COMMENT_ERROR', err.message);
            }
        });
    },

    clearErrors({LocalState}) {
        return LocalState.set('CREATE_COMMENT_ERROR', null);
    }
};
