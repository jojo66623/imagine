export default function ({Collections, Meteor}) {
    Meteor.methods({
        'message.create'(_id, userId, text, currentUserId) {
            const saving = true;
            const read = false;
            const createdAt = new Date();
            const fromId = currentUserId;
            const toId = userId;
            Collections.Messages.insert({
                _id, toId, text, fromId, saving, createdAt, read
            });
        }
    });
}
