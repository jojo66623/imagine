import React from 'react';
import {mount} from 'react-mounter';

import MainLayout from '../core/components/main_layout.jsx';
import MessageBox from './containers/message_box.js';

export default function (injectDeps, {FlowRouter}) {
    const MainLayoutCtx = injectDeps(MainLayout);

    var messageSection = FlowRouter.group({
        prefix: "/messages"
    });

    messageSection.route('/', {
        name: 'message.box',
        action() {
            mount(MainLayoutCtx, {
                content: () => (<MessageBox userId={null} />)
            });
        }
    });
    messageSection.route('/:userId', {
        name: 'message.box',
        action({userId}) {
            mount(MainLayoutCtx, {
                content: () => (<MessageBox userId={userId} />)
            });
        }
    });
}


