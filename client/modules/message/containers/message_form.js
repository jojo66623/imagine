import {useDeps, composeWithTracker, composeAll} from 'mantra-core';
import Component from '../components/message_form.jsx';

export const composer = ({context, clearErrors, userId}, onData) => {
    const {LocalState} = context();
    const error = LocalState.get('CREATE_COMMENT_ERROR');
    const currentUserId = Meteor.userId();
    onData(null, {error, currentUserId, userId});

    return clearErrors;
};

export const depsMapper = (context, actions) => ({
    create: actions.message.create,
    clearErrors: actions.message.clearErrors,
    context: () => context
});

export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMapper)
)(Component);
