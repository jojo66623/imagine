import {
  useDeps, composeWithTracker, composeAll
} from 'mantra-core';
import Component from '../components/message_list.jsx';

export const composer = ({context, clearErrors, userId}, onData) => {
    const {Meteor, Collections} = context();
    const currentUserId = Meteor.userId();
  
    if (Meteor.subscribe('message.list', currentUserId).ready()) {
        const options = {
            sort: {createdAt: 1}
        };
    
        const messages = Collections.Messages.find({$or: [{toId: userId, fromId: currentUserId},{toId: currentUserId, fromId: userId}]}, options).fetch();
    
        onData(null, {messages});
    } 
    else {
        onData();
    }
};

export default composeAll(
    composeWithTracker(composer),
    useDeps()
)(Component);
