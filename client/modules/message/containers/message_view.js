import {useDeps, composeWithTracker, composeAll} from 'mantra-core';
import Component from '../components/message_view.jsx';

export const composer = ({context, clearErrors, message}, onData) => {
    const {Meteor, Collections} = context();

    if (Meteor.subscribe('message.list', message.fromId).ready()) {
        const author = Meteor.users.findOne(message.fromId);
        if(message.toId == Meteor.userId() && message.read == false){
            Meteor.call('message.read', message._id);
        }
        onData(null, {message, author});
    } 
    else {
        onData();
    }
};

export default composeAll(
    composeWithTracker(composer),
    useDeps()
)(Component);
