import Component from '../components/message_engine.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

export const composer = ({context}, onData) => {
    const {Meteor, Collections} = context();
    const currentUserId = Meteor.userId();
    if (Meteor.subscribe('user.list').ready() && Meteor.subscribe('message.list', currentUserId).ready()) {
        const users = Meteor.users.find({ '_id': { $ne: Meteor.userId() } } );

        const options = {
            sort: {createdAt: -1}
        };

        const newMessages = Collections.Messages.find({toId: currentUserId, read: false}, options).fetch();
        const newConversations = _.groupBy(newMessages, function(conversation) { return conversation.fromId == currentUserId ? conversation.toId : conversation.fromId; });
        onData(null, {newConversations, users});
    }
    else {
        onData();
    }
};

export const depsMapper = (context, actions) => ({
    getUser: actions.user.get,
    context: () => context
});

export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMapper)
)(Component);
