import Component from '../components/message_box.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

export const composer = ({context}, onData) => {
    const {Meteor, Collections} = context();
    const currentUserId = Meteor.userId();
    if (Meteor.subscribe('message.list', currentUserId).ready()) {
        const options = {
            sort: {createdAt: -1}
        };
    
        const messages = Collections.Messages.find({$or: [{fromId: currentUserId},{toId: currentUserId}]}, options).fetch();
        const conversations = _.groupBy(messages, function(conversation) { return conversation.fromId == currentUserId ? conversation.toId : conversation.fromId; });
    
        onData(null, {conversations});
    } 
    else {
        onData();
    }
};

export default composeAll(
    composeWithTracker(composer),
    useDeps()
)(Component);