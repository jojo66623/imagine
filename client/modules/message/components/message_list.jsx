import React from 'react';
import MessageView from '../containers/message_view';

class MessageList extends React.Component {

    render() {
        const {messages} = this.props;
        return (
            <div className="message-list" ref={(c) => this._list = c}>
                { messages.map(message => (
                    <div key={message._id} className="message">
                        <MessageView message={message} />
                    </div>
                ))}
            </div>
        );
    }

    componentDidMount() {
        this._list.scrollTop = this._list.scrollHeight;
    }

    componentWillReceiveProps(nextProps) {
        this._list.scrollTop = this._list.scrollHeight;
    }
}

export default MessageList;
