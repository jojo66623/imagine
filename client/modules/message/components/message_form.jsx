import React from 'react';

class MessageForm extends React.Component {
    render() {
        const {error} = this.props;
        return (
            <div>
                { error ? this._renderError(error) : null }
                <textarea className="form-control" ref='text' placeholder='Enter your message here.'></textarea>
                <br />
                <button className="btn btn-primary" onClick={this._create.bind(this)}>Send</button>
            </div>
        );
    }

    _create() {
        const text = this.refs.text.value;
        const {create, userId, currentUserId} = this.props;
        create(userId, text, currentUserId);
        this.refs.text.value = '';
    }

    _renderError(error) {
        return (
            <div className='error'>
                { error }
            </div>
        );
    }
}

export default MessageForm;
