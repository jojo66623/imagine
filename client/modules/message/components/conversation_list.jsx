import React from 'react';
import UserTeaser from '../../user/containers/user_teaser';

const ConversationList = ({conversations}) => (
    <div className="conversation-list">
        { _.map(conversations, (conversation, userId) => (
            <div key={userId} className="conversation">
                <div className="user-teaser pull-left">
                    <UserTeaser userId={userId} />
                </div>
                <div className="pull-right">
                    { _.countBy(conversation, 'read').false > 0 ? _.countBy(conversation, 'read').false : '' }
                </div>
            </div>
        ))}
    </div>
);

export default ConversationList;