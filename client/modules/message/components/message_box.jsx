import React from 'react';
import UserTeaser from '../../user/containers/user_teaser';
import ConversationView from '../components/conversation_view.jsx';

class MessageBox extends React.Component {
  
    constructor(props) {
        super(props);
        const {userId, conversations} = this.props;
        this.state = {
            conversationId: userId ? userId : _.keys(conversations)[0]
        }
    }

    openConversation(userId) {
        this.setState({
            conversationId: userId
        });
    }

    render() {
        const {conversations} = this.props;
        return (
            <div className="message-box">
                <div className="navbar-default col-sm-3">
                    <ul className="nav nav-pills nav-stacked conversation-list">
                        { _.map(conversations, (conversation, userId) => (
                            <li key={userId} className="conversation">
                                <a onClick={this.openConversation.bind(this,userId)}>
                                    <span className="user-teaser pull-left">
                                        <UserTeaser userId={userId} />
                                    </span>
                                    <span className="pull-right">
                                        { _.countBy(conversation, function(message) { return message.toId != userId && message.read == false ? 'unread': 'others' }).unread > 0 ? 
                                            _.countBy(conversation, function(message) { return message.toId != userId && message.read == false ? 'unread': 'others' }).unread 
                                            : ''
                                        }
                                    </span>
                                </a>
                            </li>
                        ))}
                    </ul>
                </div>
                <div className="conversation col-sm-9">
                    <ConversationView userId={this.state.conversationId} />
                </div>
            </div>
        );
    }
}

export default MessageBox;
