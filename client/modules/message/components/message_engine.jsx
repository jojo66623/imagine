import React from 'react';
import ConversationView from '../components/conversation_view.jsx';

class MessageEngine extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            conversations: Session.get('conversations') ? Session.get('conversations') : []
        }
    }

    openConversation(user) {
        if (this.state.conversations.filter(function(e){ return e.id == user._id }).length == 0) {
            this.setState({
                conversations: this.state.conversations.concat({id: user._id, username: user.username, reduce: false})
            });
        }
    }

    closeConversation(index) {
        //e.preventDefault();
        let conversations = this.state.conversations;
        conversations.splice(index, 1);
        this.setState({
            conversations: conversations
        });
    }

    toggleConversation(index) {
        //e.preventDefault();
        let conversations = this.state.conversations;
        conversations[index].reduce = !conversations[index].reduce;
        this.setState({
            conversations: conversations
        });
    }

    render() {
        const {error, users} = this.props;
        const conversations = this.state.conversations;

        return (
            <div className="message-engine">
                <div className="navbar-default">
                    <ul className="nav nav-pills nav-stacked userlist">
                        { users.map(user => (
                            <li key={user._id}>
                                <a onClick={this.openConversation.bind(this,user)}>{user.username}</a>
                            </li>
                        ))}
                    </ul>
                </div>
                <ul className="list-group conversations">
                    { conversations.map((conversation,index) => (
                        <li key={index} className={"pull-left list-group-item" + (conversation.reduce ? ' reduce' : '')}>
                            <div className="conversation-title bg-primary list-group-item-heading" onClick={this.toggleConversation.bind(this,index)}>
                                { conversation.username }
                                <a onClick={this.closeConversation.bind(this,index)} className="pull-right"><i className="fa fa-times"></i></a>
                            </div>
                            <div className="conversation-content list-group-item-text">
                                <ConversationView userId={conversation.id} />
                            </div>
                        </li>
                    ))}
                </ul>
            </div>
        );
    }

    _getUser(userId) {
        const {getUser} = this.props;
        return getUser(userId);
    }

    componentWillReceiveProps(nextProps) {
        let newConversations = [];
        for(var userId in nextProps.newConversations) {
            const user = this._getUser(userId);
            if (this.state.conversations.filter(function(e){ return e.id == user._id }).length == 0){        
                newConversations = newConversations.concat({id: user._id, username: user.username, reduce: false})
            }
        }
        this.setState({
            conversations: this.state.conversations.concat(newConversations)
        });
    }

    componentDidUpdate(prevProps, prevState) {
        const conversations = this.state.conversations;
        Session.set('conversations', conversations);
    }
}

export default MessageEngine;
