import React from 'react';
import MessageForm from '../containers/message_form';
import MessageList from '../containers/message_list';

const ConversationView = ({userId}) => (
    <div className="conversation">
        <MessageList userId={userId} />
        <MessageForm userId={userId} />
    </div>
);

export default ConversationView;
