import React from 'react';

const MessageView = ({message, author}) => (
    <div>
        <b>{ author.username }:</b> { message.text }
        { message.saving ? '...' : null }
    </div>
);

export default MessageView;
