import {createApp} from 'mantra-core';
import initContext from './configs/context';

// modules
import Core from './modules/core';
import Post from './modules/post';
import Comment from './modules/comment';
import User from './modules/user';
import Message from './modules/message';
import Facebook from './modules/facebook';

// init context
const context = initContext();

// create app
const app = createApp(context);
app.loadModule(Core);
app.loadModule(Post);
app.loadModule(Comment);
app.loadModule(User);
app.loadModule(Message);
app.loadModule(Facebook);
app.init();

Accounts.ui.config({
    passwordSignupFields: "USERNAME_AND_EMAIL",
    requestPermissions: {
        facebook: ['user_friends','user_relationships','user_likes'],
    }
});
