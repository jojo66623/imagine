import {Mongo} from 'meteor/mongo';

export const Posts = new Mongo.Collection('posts');
export const Comments = new Mongo.Collection('comments');
export const Messages = new Mongo.Collection('messages');
export const Relations = new Mongo.Collection('relations');
export const FacebookData = new Mongo.Collection('facebookdata');
