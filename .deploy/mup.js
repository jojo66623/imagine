module.exports = {
  servers: {
    one: {
      host: '198.245.60.116',
      username: 'jojo',
      //pem: "/Users/Jonathan/.ssh/id_jcarly.pem"
      password: 'WieMio5y'
      // or leave blank for authenticate from ssh-agent
    }
  },

  meteor: {
    name: 'imagine',
    path: "../../imagine",
    servers: {
      one: {}
    },
    buildOptions: {
      serverOnly: true,
    },
    env: {
      ROOT_URL: 'imagine.maximetrideau.com',
      MONGO_URL: 'mongodb://localhost/meteor'
    },

    dockerImage: 'abernix/meteord:base',
    deployCheckWaitTime: 1000
  },

  mongo: {
    oplog: true,
    port: 27017,
    servers: {
      one: {},
    },
  },
};