export default function () {
    if (Meteor.users.find().count() === 0) {
        const adminId = Accounts.createUser({username: 'BigBoss', email: 'admin@mail.com', password: 'YouShallNotP@ss', role: 23, profile: {register: true}});
        Meteor.users.update(adminId, {$set: {"emails.0.verified": true}});
    }
}
