import config from './config';
import events from './events';
import initial_adds from './initial_adds';
import slingshot from './slingshot';
import social_config from './social-config';

export default function () {
    config();
    events();
    initial_adds();
    slingshot();
    social_config();
}