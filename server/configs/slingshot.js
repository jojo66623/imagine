export default function () {
    Slingshot.fileRestrictions("pictures", {
        allowedFileTypes: ["image/png", "image/jpeg", "image/gif"],
        maxSize: 10 * 1024 * 1024 // 10 MB (use null for unlimited).
    });

    Slingshot.createDirective("pictures", Slingshot.S3Storage, {
        bucket: "jcarly.bucket",

        acl: "public-read",

        authorize: function () {
            //Deny uploads if user is not logged in.
            if (!this.userId) {
                var message = "Please login before posting files";
                throw new Meteor.Error("Login Required", message);
            }

            return true;
        },

        key: function (file) {
            //Store file into a directory by the user's username.
            var user = Meteor.users.findOne(this.userId);
            return user.username + "/" + file.name;
        },

        region: 'eu-west-1',

        AWSAccessKeyId: 'AKIAINL3VWIFW47VJWKA',

        AWSSecretAccessKey: '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'
    });
}
