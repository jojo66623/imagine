import {FacebookData} from '/lib/collections';

export default function () {  

    Accounts.validateLoginAttempt(function(obj){

        if(Meteor.user()) {
            console.log('already logged');
            return false;
        }
        console.log(obj.user);
        const verified = _.some(obj.user.emails, function (email) {
            return email.verified
        });
        console.log('verified', verified);
        return verified;
    });

    Accounts.onCreateUser(function(options, user) {
        if(!options.role) {
            user.role = 1; 
        }
        if (options.profile) {
            user.profile = options.profile;
        }
        else {
            user.profile = {register: false};
        }
        if(user.services.facebook) {
            user.username = user.services.facebook.name;
            user.emails = [{address: user.services.facebook.email, verified: true}];
        }
        else {
            Meteor.setTimeout(function() {
                if(user.profile.register == false) {
                    Accounts.sendVerificationEmail(user._id);
                }
            }, 2 * 1000);
        }
    
        return user;
    });

    Accounts.validateNewUser(function (user) {
        if(this && this.userId) {
            const currentUser = Meteor.users.findOne(this.userId);
            if(currentUser) {
                if(!currentUser.services.facebook && user.services.facebook) {
                    //Accounts.findUserByEmail(user.services.facebook)
                    Accounts.addEmail(currentUser._id, user.services.facebook.email, true);
                    Meteor.users.update(currentUser._id, {
                        $set: {
                            'services.facebook': user.services.facebook
                        }
                    });

                    Meteor.defer(function(){
                        Meteor.call('facebook.getLikesData', currentUser._id, (err, result) => {
                            if (!err && result.data) {
                                FacebookData.insert({
                                    userId: currentUser._id,
                                    likes: result.data
                                });
                            }
                        });
                    });
                }
                return false;
            }
        }
        return true;
    }); 
}