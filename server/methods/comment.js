import {Comments} from '/lib/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

export default function () {
    Meteor.methods({
        'comment.create'(_id, postId, text, currentUserId) {
            check(_id, String);
            check(postId, String);
            check(text, String);
            check(currentUserId, String);

            const createdAt = new Date();
            const authorId = currentUserId;
            const comment = {_id, postId, authorId, text, createdAt};
            
            Comments.insert(comment);
        }
    });
}
