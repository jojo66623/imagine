import {Messages} from '/lib/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

export default function () {
    Meteor.methods({
        'message.create'(_id, userId, text, currentUserId) {
            check(_id, String);
            check(userId, String);
            check(text, String);
            check(currentUserId, String);

            const createdAt = new Date();
            const fromId = currentUserId;
            const toId = userId;
            const read = false;
            const message = {_id, toId, text, fromId, createdAt, read};
            console.log(message);
            Messages.insert(message);
        }
    });
    
    Meteor.methods({
        'message.read'(messageId) {
            check(messageId, String);   
            Messages.update(messageId, { $set: { read: true } });
        }
    });
}
