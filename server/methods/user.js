import {FacebookData} from '/lib/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

export default function () {
    Meteor.methods({
        'user.register'(userId, username, email, pictures) {
            check(userId, String);
            check(username, String);
            check(email, String);
            check(pictures, [String]);

            Meteor.users.update(userId, {
                $set: {
                    username: username,
                    'emails.0.address': email,
                    profile: {
                        pictures: pictures,
                        register: true
                    }
                }
            });

            Meteor.defer(function(){
                Meteor.call('facebook.getLikesData', userId, (err, result) => {
                    if (!err && result && result.data) {
                        FacebookData.insert({
                            userId: userId,
                            likes: result.data
                        });
                    }
                });
            });
        }
    });

    Meteor.methods({
        'user.update'(userId, username, email, pictures) {
            check(userId, String);
            check(username, String);
            check(email, String);
            check(pictures, [String]);

            Meteor.users.update(userId, {
                $set: {
                    username: username,
                    'emails.0.address': email,
                    profile: {
                        pictures: pictures,
                        register: true
                    }
                }
            });
        }
    });
}
