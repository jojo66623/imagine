export default function () {

    function Facebook(accessToken) {
        this.fb = require('fbgraph');
        this.accessToken = accessToken;
        this.fb.setAccessToken(this.accessToken);
        this.options = {
            timeout: 3000,
            pool: {maxSockets: Infinity},
            headers: {connection: "keep-alive"}
        }
        this.fb.setOptions(this.options);
    }

    Facebook.prototype.query = function(query, method) {
        var self = this;
        var method = (typeof method === 'undefined') ? 'get' : method;
        var data = Meteor.sync(function(done) {
            self.fb[method](query, function(err, res) {
                done(null, res);
            });
        });
        return data.result;
    }

    Facebook.prototype.getUserData = function(fbId) {
        return this.query(fbId);
    }

    Facebook.prototype.getLikesData = function(fbId) {
        var result = this.query('/'+fbId+'/likes?fields=name,link,picture,category');
        while(result.paging.next){
            const json =  HTTP.get(result.paging.next).data;
            console.log(json);
            result.paging = json.paging;
            result.data = result.data.concat(json.data);
        }
        return result;
    }

    Meteor.methods({
        'facebook.getUserData'(userId) {
            check(userId, String);
            const selector = {_id: userId};
            const user = Meteor.users.findOne(selector);
            const fb = new Facebook(user.services.facebook.accessToken);
            const data = fb.getUserData(user.services.facebook.id);

            return data;
        }
    });

    Meteor.methods({
        'facebook.getLikesData'(userId) {  
            this.unblock();
            check(userId, String);
            const selector = {_id: userId};
            const user = Meteor.users.findOne(selector); 
            if(user.services && user.services.facebook) {
                const fb = new Facebook(user.services.facebook.accessToken);
                const data = fb.getLikesData(user.services.facebook.id);
                return data;
            } 
            else {
                return null;
            }
        }
    });

    Meteor.methods({
        'facebook.updateLikesData'(userId) {  
            this.unblock();
            check(userId, String);
            const selector = {_id: userId};
            const user = Meteor.users.findOne(selector); 
            if(user.services && user.services.facebook) {
                const fb = new Facebook(user.services.facebook.accessToken);
                const data = fb.getLikesData(user.services.facebook.id);
                return data;
            } 
            else {
                return null;
            }
        }
    });
}