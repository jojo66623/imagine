import post from './post';
import comment from './comment';
import message from './message';
import facebook from './facebook';
import user from './user';

export default function () {
    post();
    comment();
    message();
    facebook();
    user();
}
