import {Posts} from '/lib/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

export default function () {
    Meteor.methods({
        'post.create'(_id, content, userId) {
            check(_id, String);
            check(content, String);
            check(userId, String);

            const createdAt = new Date();
            const authorId = userId;
            const post = {_id, content, createdAt, authorId};
            
            Posts.insert(post);
        }
    });

    Meteor.methods({
        'post.getPosts'(page = 0, nb_by_page = 10) {
            check(page, Number);
            check(nb_by_page, Number);

            const options = {
                sort: {createdAt: -1}, 
                limit: nb_by_page, 
                skip: page*nb_by_page
            };
        
            return Collections.Posts.find({}, options).fetch();
        }
    });
}
