import {Messages} from '/lib/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

export default function () {
    Meteor.publish('message.list', function (userId) {
        check(userId, String);
        const selector = {$or: [{toId: userId},{fromId: userId}]};
        
        return Messages.find(selector);
    });
}
