import {FacebookData} from '/lib/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

export default function () {
    Meteor.publish('facebook.data',  function (userId) {
        check(userId, String);
        const selector = {userId: userId};
        return FacebookData.find(selector);
    });
}
