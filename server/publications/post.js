import {Posts, Comments} from '/lib/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

export default function () {
    Meteor.publish('post.list', function () {
        const selector = {};
        const options = {
            fields: {_id: 1},
        };

        return Posts.find(selector, options);
    });

    Meteor.publish('post.view', function (postId) {
        check(postId, String);
        const selector = {_id: postId};
        
        return Posts.find(selector);
    });

    Meteor.publish('comment.list', function (postId) {
        check(postId, String);
        const selector = {postId};
        
        return Comments.find(selector);
    });
}
