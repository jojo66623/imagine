import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

export default function () {
    Meteor.publish('user.list', function () {
        return Meteor.users.find({});
    });

    Meteor.publish('user.view', function (userId) {
        check(userId, String);
        const selector = {_id: userId};
        
        return Meteor.users.find(selector);
    });

    Meteor.publish('user.current', function () {
        return Meteor.users.find({_id: this.userId});
    });
}