import post from './post';
import user from './user';
import message from './message';
import facebook from './facebook';

export default function () {
	user();
	post();
	message();
	facebook();
}
